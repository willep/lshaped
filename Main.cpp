#include "LShapedMesh.h"

int main() {
  Mesh msh = LShapedMesh(1.0, 30);
  msh.write("Plots/Mesh.npz");
  Equation e = poisson(msh.dim);
  
  // Set b to be the right-hand side
  DArray b(solnSize(msh, e));
  b = -1.0;
  mass(b, msh);
  
  // Solve using 0 initial guess for Newton
  DArray u(solnSize(msh, e));
  u = 0.0;
  steadySolve(u, b, msh, e);
  tnp::write(u, "Plots/u0000.dat");
  
  Timer::report();
  return 0;
}
