#include "LShapedMesh.h"

Mesh LShapedMesh(double h, int p) {
  Mesh msh;
  
  int n = 1.0/h;
  int nel = 3*n*n;
  int nv = (n+1)*(2*n+1) + (n+1)*n;
  int dim = 2;
  
  msh.init(dim, nel, nv);
  
  // Place first layer of vertices
  for (int iy=0; iy<n+1; ++iy)
  for (int ix=0; ix<2*n+1; ++ix) {
    int iv = ix + iy*(2*n+1);
    msh.v(0,iv) = ix*h;
    msh.v(1,iv) = iy*h;
  }
  // Place second layer of vertices
  for (int iy=0; iy<n; ++iy)
  for (int ix=0; ix<n+1; ++ix) {
    int iv = ix + iy*(n+1) + 2*n + 1 + n*(2*n+1);
    msh.v(0,iv) = ix*h;
    msh.v(1,iv) = 1 + (iy+1)*h;
  }
  
  // Place first layer of elements
  for (int iy=0; iy<n; ++iy)
  for (int ix=0; ix<2*n; ++ix) {
    int iel = ix + iy*2*n;
    int iv = ix + iy*(2*n+1);
    
    msh.el(0,iel) = iv;
    msh.el(1,iel) = iv+1;
    msh.el(2,iel) = iv + (2*n+1);
    msh.el(3,iel) = iv + (2*n+1) + 1;
  }
  
  // Place transition elements
  for (int ix=0; ix<n; ++ix) {
    int iel = ix + 2*n + 2*(n-1)*n;
    int iv = ix + n*(2*n+1);
    
    msh.el(0,iel) = iv;
    msh.el(1,iel) = iv+1;
    msh.el(2,iel) = iv + (2*n+1);
    msh.el(3,iel) = iv + (2*n+1) + 1;
  }
  
  // Place second layer of elements
  for (int iy=0; iy<n-1; ++iy)
  for (int ix=0; ix<n; ++ix) {
    int iel = ix + iy*n + 3*n + 2*(n-1)*n;
    int iv = ix + iy*(n+1) + n*(2*n+1) + 2*n+1;
    
    msh.el(0,iel) = iv;
    msh.el(1,iel) = iv+1;
    msh.el(2,iel) = iv + (n+1);
    msh.el(3,iel) = iv + (n+1) + 1;
  }
  
  msh.formAdjacency();
  msh.precompute(p);
  return msh;
}
